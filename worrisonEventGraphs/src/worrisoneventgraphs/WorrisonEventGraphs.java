/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worrisoneventgraphs;

import Models.Crime;
import Models.Entity;
import Models.Graph;
import Models.Neighborhood;

/**
 *
 * @author LinaM
 */
public class WorrisonEventGraphs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        // Grafo para insersion de Barrios y sus relaciones(Arcos)
        Graph graph = new Graph(false); 
        graph.insert(new Neighborhood("100", "Fatima", 1));
        graph.insert(new Neighborhood("101", "Bosque", 1));
        graph.insert(new Neighborhood("102", "Palermo", 1));
        graph.insert(new Neighborhood("103", "Chipre", 1));
        
        graph.insertArc("Fatima", "Bosque", 1);
        graph.insertArc("Palermo", "Chipre", 4);
        

        //Grafo para insertar delitos 
        
        Graph crime = new Graph(true);
        
        crime.insert(new Crime("200", "Robo"));
        crime.insert(new Crime("201", "Violacion"));
        crime.insert(new Crime("202", "Drogas"));
        crime.insert(new Crime("203", "Violencia Intrafamiliar"));
        
        crime.insertArc("Robo", "Drogas", 0);
        crime.insertArc("Violacion", "Violencia Intrafamiliar", 0);
    }
    
}
