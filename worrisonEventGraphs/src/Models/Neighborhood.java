/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author LinaM
 */
public class Neighborhood extends Entity{
    
    protected int stratum;
    
    public Neighborhood(String code, String name, int stratum) {
        super(code, name);
        this.stratum = stratum;
    }

    public int getStratum() {
        return stratum;
    }

    public void setStratum(int stratum) {
        this.stratum = stratum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
