/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * Clase Relacion para relacionar los vertices del grafo
 */
public class Relationship{
    
    
    private Entity entity;
    
    //Para Barrios: Peso o distancia entre barrios
    //Para Delitos: Contador de numero de delitos cometidos en un barrio
    private int weight; 
    
    //Bandera para señalar si un vertice es visitado en una busqueda
    private boolean visited;

    public Relationship(Entity entity, int weight) {
        this.entity = entity;
        this.weight = weight;
    }
    
    
    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

   }
