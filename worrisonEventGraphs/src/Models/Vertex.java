/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;


import java.util.ArrayList;
/**
 *
 * @author LinaM
 */
public class Vertex {
    
    private Entity entity;
    
    //Array de Vecinos/Relaciones entre Vertices
    private ArrayList<Relationship> relationsShip ; 
 
    public Vertex(Entity entity){
        this.entity  = entity;
        this.relationsShip  = new ArrayList<>();
    }
    
    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public ArrayList<Relationship> getRelationsShip() {
        return relationsShip;
    }

    public void setRelationsShip(ArrayList<Relationship> relationsShip) {
        this.relationsShip = relationsShip;
    }
    
    //Metodo de busqueda de Relaciones entre Vertices 
    public boolean findR(String name){
        
        for (Relationship element : relationsShip) {
            if (element.getEntity().getName().equalsIgnoreCase(name)) {
                return true;
            } 
        }
        
        return false;
    }
    
}
