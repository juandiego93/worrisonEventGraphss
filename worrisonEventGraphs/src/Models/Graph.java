/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;


import java.util.ArrayList; 
/**
 *
 * @author LinaM
 */
public class Graph {
   
    //Array de lista de Vertices/Barrios del barrio
    ArrayList<Vertex> Vertex ;
    //Bandera para indicar el tipo de Grafo (Dirigido: true, No dirigido:false)
    boolean addressed ;

    //Constructor
    public Graph(boolean addressed){
        this.Vertex = new ArrayList<>();
        this.addressed = addressed;
    }

    public boolean isAddressed() {
        return addressed;
    }

    public void setAddressed(boolean addressed) {
        this.addressed = addressed;
    }
    
    // Metodo de busqueda para buscar un barrio dentro del grafo.
    //Devuelve Verdadero o Falso.
    public Vertex find(String name){  
        for (Vertex vertex : Vertex) {
            if (vertex.getEntity().getName() == name) {                
                System.out.println("Vertice Encontrado");
                return vertex;
            }            
            else{
                System.out.println("Vertice No Existente");
            }
        } 
        
        return null;
    }   
    
    //Metodo de Insersion de Barrios para el grafo
    //Devuelve Verdadero o Falso.
    public boolean insert(Entity entity){
        
        Vertex vertex = this.find(entity.getName());
         
        if (vertex == null) {
            Vertex newVertex = new Vertex(entity);
            this.Vertex.add(newVertex);
            System.out.println("Vertie insertado");
            return true;
        }     
         return false;
    }
    
    //Metodo para la insersion de Relaciones entre Vertices 
    public boolean insertArc(String nameVerInitial, String nameVertRelation, int weight){
        
        Vertex tempVertexA = this.find(nameVerInitial);
        
        if(tempVertexA != null){
            
            Vertex tempVertexB = this.find(nameVertRelation);
            
            if(tempVertexB != null){
                if (!tempVertexA.findR(nameVertRelation)){ 
                        Entity entityA = tempVertexB.getEntity();
                        Relationship relationshipA = new Relationship(entityA, weight); 
                        tempVertexA.getRelationsShip().add(relationshipA);
                    if(!this.isAddressed()){
                        Entity entityB = tempVertexA.getEntity();
                        Relationship relationshipB = new Relationship(entityB, weight);
                        tempVertexB.getRelationsShip().add(relationshipB);
                    }
                    return true;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        
        return false;
    }
 
    //Metodo para la eliminacion de una relacion entre Vertices
    public boolean deleteArc(String nameVerInitial, String nameVertRelation){
        
        Vertex tempVerInitial = this.find(nameVerInitial);
        
        if (tempVerInitial != null) {
            
        }
        return false;
    } 
    
    //Metodo para eliminar relacion entre Vertices
    public boolean deletArc(String vertInitial, String vertSecond, String nameRelation){
        
        Vertex tempVertexInitial = this.find(vertInitial);
        
        if (tempVertexInitial != null) {
            
            Vertex temVertexSecond = this.find(vertSecond);
            if (temVertexSecond != null){
                tempVertexInitial.getRelationsShip().remove(nameRelation);
                return true;
            }
            if (!this.addressed) {
                temVertexSecond.getRelationsShip().remove(nameRelation);
                return true;
            }
        }
        
        return true;
    }
    
    //buscar peso 
    
}
