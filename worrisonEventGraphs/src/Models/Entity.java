/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * Clase abstracta para indicar el comportamiento 
 * (Atributos y metodos que deben tener los hijos de esta clase)
 */
public abstract class Entity {
    
    protected String code;
    protected String name;
    
    public Entity(String code, String name){
        this.code = code;
        this.name = name; 
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
}
